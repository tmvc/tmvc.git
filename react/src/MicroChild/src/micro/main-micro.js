import React from 'react';
import ReactDOM from 'react-dom';
import App from '../../../App';
import registerServiceWorker from '../../../registerServiceWorker';
import '@babel/polyfill'
import { HashRouter } from 'react-router-dom'
import { Provider } from 'mobx-react'
import { LocaleProvider } from 'antd'
import zh_CN from 'antd/lib/locale-provider/zh_CN'
import store from '../../../store'

// import Cookies from 'js-cookie'
/**
 * name 导入官方通信方法
 */
// import appStore from './app-store/app-store'
// 判断是否在微应用试用下
// const __qiankun__ = window.__POWERED_BY_QIANKUN__
const __qiankun__ = window.__POWERED_BY_QIANKUN__

/**
 * name 导出生命周期函数
 */
const microMain = () => {
  return {
    /**
     * name 微应用初始化
     * param {Object} props 主应用下发的props
     * description  bootstrap 只会在微应用初始化的时候调用一次，下次微应用重新进入时会直接调用 mount 钩子，不会再重复触发
     * description 通常我们可以在这里做一些全局变量的初始化，比如不会在 unmount 阶段被销毁的应用级别的缓存等
     */
    async bootstrap(props = {}) {
      console.log(props)
      // 存储全局使用 props是基座传输过来的值，可进行token等存储使用
      // Vue.prototype.$MicroBootstrap = props
      // Cookies.set(props,'$MicroBootstrap')
    },
    /**
     * name 实例化微应用
     * param {Object} props 主应用下发的props
     * description 应用每次进入都会调用 mount 方法，通常我们在这里触发应用的渲染方法
     */
    async mount(props) {
      console.log(props)

      // 注册应用间通信  props是基座传输过来的值，可进行token等存储使用
      // appStore(props)// 存储例子1 可作参考
      // Vue.prototype.$MicroMount = props
      // Cookies.set(props,'$MicroMount')
      // 注册微应用实例化函数
      render(props)
    },
    /**
     * name 微应用卸载/切出
     */
    async unmount() {
      // instance.$destroy()
      // instance.$el.innerHTML = ''
      // instance = null
    },
    /**
     * name 手动加载微应用触发的生命周期
     * param {Object} props 主应用下发的props
     * description 可选生命周期钩子，仅使用 loadMicroApp 方式手动加载微应用时生效
     */
    async update(props) {
      console.log('update props', props)
    }
  }
}

/**
 * name 子应用实例化函数
 * param {Object} props param0 qiankun将用户添加信息和自带信息整合，通过props传给子应用
 * description {Array} routes 主应用请求获取注册表后，从服务端拿到路由数据
 * description {String} 子应用路由前缀 主应用请求获取注册表后，从服务端拿到路由数据
 */
const render = ({ name, container } = {}) => {
  console.log(name,container)
  ReactDOM.render(
    <HashRouter forcerefresh={true}>
      <LocaleProvider locale={zh_CN}>
        <Provider {...store}>
          <App />
        </Provider>
      </LocaleProvider>
    </HashRouter>,
    container?container.querySelector('#roots'):document.querySelector('#roots'))

}
registerServiceWorker();

// 独立运行环境直接实例化vue
__qiankun__ || render()
export { microMain, render }
