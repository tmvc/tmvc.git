import '@babel/polyfill'
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { HashRouter } from 'react-router-dom'
import { Provider } from 'mobx-react'
import { LocaleProvider } from 'antd'
import zh_CN from 'antd/lib/locale-provider/zh_CN'
import store from './store'
import './public-path'
// 微前端配置文件注入
import tmvc from './MicroChild/src/index.js'
//打包时，用的HashRouter并加上了basename，因为放在服务器的二级目录下

  const render = container => {
    ReactDOM.render(
      <HashRouter forcerefresh={true}>
        <LocaleProvider locale={zh_CN}>
          <Provider {...store}>
            <App />
          </Provider>
        </LocaleProvider>
      </HashRouter>,
      container?container.querySelector('#roots'):document.querySelector('#roots'))
   }
  if (!window.__POWERED_BY_QIANKUN__) {
    render();
  }
  
  // If you want your app to work offline and load faster, you can change
  // unregister() to register() below. Note this comes with some pitfalls.
  // Learn more about service workers: https://bit.ly/CRA-PWA
  
registerServiceWorker();

// tmvc.publicPath()
// // name 导出微应用生命周期
const { bootstrap, mount, unmount } = tmvc.microMain()
export { bootstrap, mount, unmount }